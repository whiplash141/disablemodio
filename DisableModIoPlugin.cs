﻿using Sandbox;
using Sandbox.Engine.Networking;
using Sandbox.Game.Screens.ViewModels;
using Sandbox.Graphics.GUI;
using SpaceEngineers.Game.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using VRage.GameServices;
using VRage.Mod.Io;
using VRage.Plugins;
using VRage.Steam;

namespace DisableModIo
{
    public class DisableModIoPlugin : IPlugin
    {
        public void Dispose()
        {

        }

        public void Update()
        {

        }

        public void Init(object gameInstance)
        {
            MySandboxGame.Log.WriteLine("###PLUGIN: DISABLE MOD.IO: Load Start");
            MySandboxGame.Config.ModIoConsent = false; // no.
            MySandboxGame.Log.WriteLine("###PLUGIN: DISABLE MOD.IO: Forced Config.ModIoConsent to false");

            // Stub consent spamming logic
            var targetInfo = typeof(MyGameService).GetProperty("ShouldAskForConsent");
            var targetMethod = targetInfo.GetGetMethod();
            var patchedInfo = typeof(DisableModIoPlugin).GetProperty("StubShouldAskForConsent");
            var patchedMethod = patchedInfo.GetGetMethod();
            MethodUtil.ReplaceMethod(patchedMethod, targetMethod);
            MySandboxGame.Log.WriteLine("###PLUGIN: DISABLE MOD.IO: Stubbed MyGameService.ShouldAskForConsent");

            // Find ModIo assembly and class type
            var modIoAssembly = typeof(MyModIoService).Assembly;
            var modIoClassType = modIoAssembly.GetType("VRage.Mod.Io.MyModIo");

            // Remove anything that isn't Steam from the UGCAggregator list
            var targetField = typeof(MyUGCAggregator).GetField("m_aggregates", BindingFlags.NonPublic | BindingFlags.Instance);
            List<IMyUGCService> ugcServices = (List<IMyUGCService>)(targetField.GetValue(MyGameService.WorkshopService));
            for (int i = ugcServices.Count - 1; i >= 0; --i)
            {
                var service = ugcServices[i];
                if (service.ServiceName.ToUpperInvariant() != "STEAM")
                {
                    MySandboxGame.Log.WriteLine($"###PLUGIN: DISABLE MOD.IO: Removing UGC service '{service.ServiceName}'");
                    ugcServices.RemoveAt(i);
                }
            }

            foreach (var service in ugcServices)
            {
                MySandboxGame.Log.WriteLine($"###PLUGIN: DISABLE MOD.IO: Remaining UGC services '{service.ServiceName}'");
            }

            // The following are not necessary as they _should_ never run, but just in case...

            // Stub account linking logic
            targetMethod = modIoClassType.GetMethod("LinkAccount");
            patchedMethod = typeof(DisableModIoPlugin).GetMethod("StubLinkAccount");
            MethodUtil.ReplaceMethod(patchedMethod, targetMethod);
            MySandboxGame.Log.WriteLine("###PLUGIN: DISABLE MOD.IO: Stubbed ModIo.LinkAccount");

            // Stub email exchange 
            targetMethod = modIoClassType.GetMethod("EmailExchange");
            patchedMethod = typeof(DisableModIoPlugin).GetMethod("StubEmailExchange");
            MethodUtil.ReplaceMethod(patchedMethod, targetMethod);
            MySandboxGame.Log.WriteLine("###PLUGIN: DISABLE MOD.IO: Stubbed ModIo.EmailExchange");

            // Stub email request 
            targetMethod = modIoClassType.GetMethod("EmailRequest");
            patchedMethod = typeof(DisableModIoPlugin).GetMethod("StubEmailRequest");
            MethodUtil.ReplaceMethod(patchedMethod, targetMethod);
            MySandboxGame.Log.WriteLine("###PLUGIN: DISABLE MOD.IO: Stubbed ModIo.EmailRequest");

            // EmptyKeys.UserInterface.Generated.ModIoConsentView

            MySandboxGame.Log.WriteLine("###PLUGIN: DISABLE MOD.IO: Load Complete");
        }

        public static bool StubShouldAskForConsent
        {
            get
            {
                MySandboxGame.Log.WriteLine("###PLUGIN: DISABLE MOD.IO: Blocked modio consent screen spam");
                return false;
            }
        }

        public static Type type = typeof(bool);

        public static void StubLinkAccount(string email, Action<MyGameServiceCallResult> onDone)
        {
            MySandboxGame.Log.WriteLine("###PLUGIN: DISABLE MOD.IO: Blocked ModIo.LinkAccount");
        }

        public static void StubEmailExchange(string code, Action<MyGameServiceCallResult> onCompleted)
        {
            MySandboxGame.Log.WriteLine("###PLUGIN: DISABLE MOD.IO: Blocked ModIo.EmailExchange");
        }

        public static void StubEmailRequest(string email, Action<MyGameServiceCallResult> onCompleted)
        {
            MySandboxGame.Log.WriteLine("###PLUGIN: DISABLE MOD.IO: Blocked ModIo.EmailRequest");
        }
    }
}
