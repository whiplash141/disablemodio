# UPDATE: v1.197.168 (2021/02/12)
## **ModIo TOS acceptance is no-longer required in order to access the blueprint menu or steam workshop content. This plugin is now defunct and will be placed in readonly mode.**

-----

# (Legacy README - No longer relevant)

## Background

The most recent patch (1.197.168) added a new TOS screen whenever you try to access certain menus that prompts you to accept mod.io's TOS before being able to use _any_ workshop content.

You can **not** opt out of one service of another. You MUST accept mod.io's TOS to use steam content. **You also MUST accept mod.io's TOS to use your own blueprint menu.**

Players should not be forced to accept the TOS of a service that they do not wish to use just to be able to access their own **blueprint menu**!

---------------------

https://i.imgur.com/JrVSfJz.jpg

The text reads:

> We use Steam and mod.io to support user-generated content in-game. To continue you must agree to the Steam and mod.io Terms of Use and Privacy Policy. A mod.io account will be created for you to browse, share and interact with user-generated content using your:

> •   Steam User ID

> •   Display name

> •   Avatar

> You agree that your display name, avatar and content will be shared through the mod.io API and website which may be publicly accessible. You can manage your mod.io account and opt out any time via mod.io.

> Please read Terms of Use and accept them.

---------------------

## (Hopefully) Temporary Solution

To remedy this insanity, I've created a plugin that **disables mod.io integration** and allows you to access steam generated content and your own bloody blueprints without being blocked by the mod.io TOS.

Release: https://gitlab.com/whiplash141/disablemodio/-/releases/v1.0.0

Source code: https://gitlab.com/whiplash141/disablemodio/-/tree/master

# What precisely does this plugin do?

* It removes mod.io from the list of content aggregators that the game uses
* It stubs out any code that communicates your email or username to mod.io
* It removes the obtrusive, forced consent screen that blocks you from accessing your blueprints menu

With this plugin installed, the game should run as it did before this update. This will likely not be compatible with the XBOX crossplay.

## Installation
You can either build `DisableModIo.dll` from the source code using Visual Studio, or use the zip file with the .dll already compiled.

1. Place the `DisableModIo.dll` plugin wherever you want
2. In Steam:
    1. Right click "Space Engineers"
    2. Select "Properties"
    3. Select the "General" tab
3. Add the following to the "LAUNCH OPTIONS":

`-plugin "C:/path/to/DisableModIo.dll"` - Remember to use quotes around the path to avoid any issues (`"`).

4. Then launch your game, and you shouldn't be bothered by the mod.io TOS page again.

**NOTE**: If the plugin causes the game to crash you either:

* Are running on a version of the game before 1.197.168

* Need to unblock `DisableModIo.dll` by right clicking the file, selecting properties, then unblocking


---------------------

## Afterword

The game was seemingly designed to support enabling either mod.io, Steam, or both. The UI automagically updates when one or the other is removed. It does not make sense to me why Keen opted to go this way, and the implementation is anti-player.

Hopefully Keen remedies this soon so this plugin isn't needed.
The game was seemingly designed to support enabling either mod.io, Steam, or both. The UI automagically updates when one or the other is removed. It does not make sense to me why Keen opted to go this way, and the implementation is anti-player.

Hopefully Keen remedies this soon so this plugin isn't needed.
