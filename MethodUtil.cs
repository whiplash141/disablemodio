﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;

namespace DisableModIo
{
    /// <summary>
    /// Stolen... err.. borrowed from Malware's PBUnlimiter plugin.
    /// </summary>
    public static class MethodUtil
    {
        public static void ReplaceMethod(MethodBase source, MethodBase dest, bool UNSAFE = false)
        {
            if (!UNSAFE && !MethodSignaturesEqual(source, dest))
            {
                throw new ArgumentException("The method signatures are not the same.", "source");
            }
            ReplaceMethod(GetMethodAddress(source), dest);
        }

        public unsafe static void ReplaceMethod(IntPtr srcAdr, MethodBase dest)
        {
            IntPtr methodAddress = GetMethodAddress(dest);
            if (IntPtr.Size == 8)
            {
                ulong* ptr = (ulong*)methodAddress.ToPointer();
                *ptr = (ulong)(*(long*)srcAdr.ToPointer());
            }
            else
            {
                uint* ptr2 = (uint*)methodAddress.ToPointer();
                *ptr2 = *(uint*)srcAdr.ToPointer();
            }
        }

        public unsafe static IntPtr GetMethodAddress(MethodBase method)
        {
            if (method is DynamicMethod)
            {
                return GetDynamicMethodAddress(method);
            }
            RuntimeHelpers.PrepareMethod(method.MethodHandle);
            return new IntPtr((byte*)method.MethodHandle.Value.ToPointer() + 2L * 4L);
        }

        private unsafe static IntPtr GetDynamicMethodAddress(MethodBase method)
        {
            byte* ptr = (byte*)GetDynamicMethodRuntimeHandle(method).Value.ToPointer();
            if (IntPtr.Size == 8)
            {
                ulong* ptr2 = (ulong*)ptr;
                ptr2 += 6;
                return new IntPtr(ptr2);
            }
            uint* ptr3 = (uint*)ptr;
            ptr3 += 6;
            return new IntPtr(ptr3);
        }

        private static RuntimeMethodHandle GetDynamicMethodRuntimeHandle(MethodBase method)
        {
            if (method is DynamicMethod)
            {
                FieldInfo field = typeof(DynamicMethod).GetField("m_method", BindingFlags.Instance | BindingFlags.NonPublic);
                if (field != null)
                {
                    return (RuntimeMethodHandle)field.GetValue(method);
                }
            }
            return method.MethodHandle;
        }

        private static bool MethodSignaturesEqual(MethodBase x, MethodBase y)
        {
            if (x.CallingConvention != y.CallingConvention)
            {
                return false;
            }
            Type methodReturnType = GetMethodReturnType(x);
            Type methodReturnType2 = GetMethodReturnType(y);
            if (methodReturnType != methodReturnType2)
            {
                return false;
            }
            ParameterInfo[] parameters = x.GetParameters();
            ParameterInfo[] parameters2 = y.GetParameters();
            if (parameters.Length != parameters2.Length)
            {
                return false;
            }
            for (int i = 0; i < parameters.Length; i++)
            {
                if (parameters[i].ParameterType != parameters2[i].ParameterType)
                {
                    return false;
                }
            }
            return true;
        }

        private static Type GetMethodReturnType(MethodBase method)
        {
            MethodInfo methodInfo = method as MethodInfo;
            if (methodInfo == null)
            {
                throw new ArgumentException("Unsupported MethodBase : " + method.GetType().Name, "method");
            }
            return methodInfo.ReturnType;
        }
    }
}
